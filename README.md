# Node app with Passport authentication

## Useful links

Page styling thanks to the themes provided by [Bootswatch](https://bootswatch.com/lux/)

Additional front-end usability wouldn't be possible without: 

* [Bootstrap](https://getbootstrap.com 'Bootstrap javascript')

Backend functionality relies on these awesome frameworks:

* [Node](https://nodejs.org)
* [Express](https://expressjs.com)
* [EJS](https://ejs.co/)
