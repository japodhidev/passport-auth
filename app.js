const express = require('express');
const expressLayouts = require('express-ejs-layouts');
const passport = require('passport');
const Strategy = require('passport-local').Strategy;
const session = require('express-session');
const sessionStore = require('connect-mongodb-session')(session);

const app = express();

// EJS templating engine
app.use(expressLayouts);
app.set('view engine', 'ejs');

// Static files
app.use(express.static('static'));

// TODO: Set session variables/properties

// Loggin, session handling and parsing
app.use(require('morgan')('combined'));
app.use(require('cookie-parser')());
app.use(require('body-parser').urlencoded({ extended: true }));

// Set custom headers
// app.use((req, res, next) => {
//   res.setHeader("Access-Control-Allow-Origin", "*");
// });

// MongoDB session store
var store = new sessionStore({
  uri: 'mongodb+srv://japodhi:JapodhiIsDev13!@cluster0-h6qyt.mongodb.net/test?retryWrites=true',
  collection: 'userdetails'
});

// Catch errors
store.on('error', function(error){
  assert.ifError(error);
  assert.ok(false);
});

app.use(require('express-session')({
    secret: 'keyboard cat', resave: false,
    saveUninitialized: false,
    store: store,
    resave: true,
    saveUninitialized: true
  })
);


// Routes
app.use('/', require('./routes/index'));
app.use('/user', require('./routes/users'));


const PORT = process.env.PORT || 5000;

app.listen(PORT, console.log(`Serving at table ${PORT}`));
