var mongoose = require('mongoose');

const mongodb  = 'mongodb+srv://japodhi:JapodhiIsDev13!@cluster0-h6qyt.mongodb.net/test?retryWrites=true';

const UserDets = new mongoose.Schema({
    firstName: String,
    secondName: String,
    email: String,
    password: String,
    dateRegistered: {
      type: Date,
      default: Date.now()
    }
});

var conn = mongoose.connect(mongodb, {useNewUrlParser: true});
//conn.Promise = global.Promise;
var db = mongoose.connection;

const userModel = mongoose.model('userdetail', UserDets);

// Bind the connection to an error event
db.on('error', console.error.bind(console, 'MongoDB connection error'));

// const userdetail = mongoose.model('userdetail', userdets);

exports.saveDetail = async function (dets) {
  userModel.create(dets);
  await db.userModel.save().then(async() => {
    await console.log('User details saved');
  }).catch((err) => {
    if (err) {
      console.log(err);
    }
  });
  db.close(() => {
    console.log("[+] Database connection terminated successfully.");
  });
};
module.exports = userModel;
