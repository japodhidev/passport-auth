const express = require('express');
const passport = require('passport');
const Strategy = require('passport-local').Strategy;

const router = express.Router();

// Homepage view
router.get('/', (req, res) => res.render('home'));

// Dashboard View
router.get('/dashboard', require('connect-ensure-login').ensureLoggedIn('/user/login'),
function(req, res){
  res.render('dashboard', {user: req.user});
});

module.exports = router;
