const express = require('express');
const {check, validationResult} = require('express-validator/check');
const passport = require('passport');
const Strategy = require('passport-local').Strategy;

// brcyptjs
const bcrypt = require('bcryptjs');
const salt = bcrypt.genSaltSync(10);

const userDetail = require('../models/schema');

const router = express.Router();

// Passport Strategy
passport.use(new Strategy(
  function(email, password, cb) {
    // TODO: Find user details from db
    userDetail.userModel.findOne({email: email}, function(err, user) {
      if (err) { return cb(err); }
      if (!user) { return cb(null, false); }

      // match password
      bcrypt.compare(password, user.password, (err, isMatch) => {
        if (err) throw err;

        if (isMatch) {
          return done(null, user);
        } else {
          return done(null, false, {message: 'Password Incorrect'});
        }
      });

      return done(null, user);
    });
  }
));

// Store authenticated users in and out of the sessionStore
passport.serializeUser(function(user, cb){
  cb(null, user.id);
});

passport.deserializeUser(function(id, cb){
  userDetail.userModel.findById(id, function(err, user){
    if (err) { return cb(err); }
    cb(null, user);
  });
});

// Login view
router.get('/', (req, res) => res.render('user'));

// Login view
router.get('/login', (req, res) => res.render('login'));

// Login form post
router.post('/login', [
  check('email').isEmpty()
], (req, res) => {
  const loginError = validationResult(req);
  if (!loginError.isEmpty()) {
    return res.status(422).json({
      errors: loginError.array()
    });
  } else {
    passport.authenticate('local', {
      successReturnToOrRedirect: '/dashboard', failureRedirect: '/user/login'
    });
    // TODO: Some form of encryption for the password before pushing to collection
    res.redirect(301, '/dashboard/')
  }
});

// Register view
router.get('/register', (req, res) => res.render('register'));

// Process register form
router.post('/register_dets', [
  check('email').isEmail(),
  check('password').isLength({min: 8})
],
(req, res) => {
  const error = validationResult(req);
  if (!error.isEmpty()) {
    return res.status(422).json({
      errors: error.array()
    });
  } else {
    // TODO: Sanitize & push data to collection before redirect
    var email = req.body.email;
    var firstName = req.body.firstname;
    var secondName = req.body.secondname;
    var password = req.body.password;

    const dets = {
      firstName: firstName,
      secondName: secondName,
      email: email,
      password: bcrypt.hashSync(password, salt)
    };

    userDetail.saveDetail(dets);

    res.redirect(301, '/user/login/');
  }

});

module.exports = router;
